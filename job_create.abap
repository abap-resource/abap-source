 CALL FUNCTION 'JOB_OPEN'
    EXPORTING
      jobname          = jobname
    IMPORTING
      jobcount         = jobcount
    EXCEPTIONS
      cant_create_job  = 1
      invalid_job_data = 2
      jobname_missing  = 3
      OTHERS           = 4.
  IF sy-subrc <> 0.
    MESSAGE e888(sabapdocu) WITH 'JOB_OPEN FAILED'.
    EXIT.
  ENDIF.
 
  CALL FUNCTION 'JOB_SUBMIT'
    EXPORTING
      authcknam               = sy-uname
      jobcount                = jobcount
      jobname                 = jobname
      report                  = program
      variant                 = ''
    EXCEPTIONS
      bad_priparams           = 1
      bad_xpgflags            = 2
      invalid_jobdata         = 3
      jobname_missing         = 4
      job_notex               = 5
      job_submit_failed       = 6
      lock_failed             = 7
      program_missing         = 8
      prog_abap_and_extpg_set = 9
      OTHERS                  = 10.
  IF sy-subrc <> 0.
    MESSAGE e888(sabapdocu) WITH 'JOB_SUBMIT FAILED'.
    EXIT.
  ENDIF.
 
  CALL FUNCTION 'JOB_CLOSE'
    EXPORTING
      jobcount             = jobcount
      jobname              = jobname
      strtimmed            = 'X'
    EXCEPTIONS
      cant_start_immediate = 1
      invalid_startdate    = 2
      jobname_missing      = 3
      job_close_failed     = 4
      job_nosteps          = 5
      job_notex            = 6
      lock_failed          = 7
      invalid_target       = 8
      OTHERS               = 9.
  IF sy-subrc <> 0.
    MESSAGE e888(sabapdocu) WITH 'JOB_CLOSE FAILED'.
    EXIT.
  ENDIF.